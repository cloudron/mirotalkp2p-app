FROM cloudron/base:5.0.0@sha256:04fd70dbd8ad6149c19de39e35718e024417c3e01dc9c6637eaf4a41ec4e596c

RUN mkdir -p /app/code /app/pkg
WORKDIR /app/code

# node 22 - https://github.com/miroslavpejic85/mirotalk/blob/master/Dockerfile#L2
RUN node -v | grep -q 'v22' || (echo "Requires node v22" && exit 1)

# renovate: datasource=custom.mirotalkp2p depName=mirotalkp2p versioning=semver
ARG MIROTALKP2P_VERSION=1.4.85

# version is at https://github.com/miroslavpejic85/mirotalk/blame/master/package.json#L3
# commit is patched up by renovate post upgrade task
ARG MIROTALKP2P_COMMIT=47f731afd78233d29f4cb5bae5918c0eabb497a7

RUN curl -L https://github.com/miroslavpejic85/mirotalk/archive/${MIROTALKP2P_COMMIT}.tar.gz | tar -xz --strip-components 1 -f - -C .
RUN npm i

RUN mv /app/code/.env.template /app/pkg/env.template
RUN ln -sf /run/env /app/code/.env

# code expects without .js for now!
COPY config.js /app/code/app/src/config
COPY start.sh /app/pkg/

WORKDIR /app/pkg
CMD [ "/app/pkg/start.sh" ]
