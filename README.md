# cloudron-app-package
Package for deloyment of this app on Cloudron

## THIS IS ONLY A PROJECT TO MAKE IT POSSIBLE TO DEPLOY THIS APP ON CLOUDRON 

To deploy it manually on Cloudron follow this instruction:

You need to clone this repo to a local linux where the [Cloudron CLI](https://docs.cloudron.io/packaging/cli/) and Docker is installed. On your cloudron you can install the [Docker Registry App](https://docs.cloudron.io/apps/docker-registry/) to host the custom Image. In Cloudron itself you need to [configure your custom Docker Registry](https://docs.cloudron.io/settings/#private-docker-registry).

Build the docker Image and give it a Name:
~~~
docker build -t APPNAME .
~~~

Tag the docker Image and specify the URL of the Docker Registry that you will push it to (you may install the Docker Registry App in Cloudron):
~~~
docker tag APPNAME:latest URL-OF-YOUR-IMAGE-REGISTRY/APPNAME
~~~

Push Image to your Docker Registry:
~~~
docker push URL-OF-YOUR-IMAGE-REGISTRY/APPNAME
~~~

Install image on your Cloudron Instance with the URL of the Docker Registry:
~~~
cloudron install --image URL-OF-YOUR-IMAGE-REGISTRY/APPNAME
~~~

## License
This project is under the [GNU GPLv3](LICENSE).

