## About

Simple, Secure, Fast Real-Time Video Conferences Up to 4k and 60fps, compatible with all browsers and platforms.

## License

MiroTalk P2P is free and open-source under the terms of AGPLv3 (GNU Affero General Public License v3.0).

To obtain a MiroTalk P2P license with terms different from the AGPLv3, you can conveniently make your purchase on [CodeCanyon](https://codecanyon.net/user/miroslavpejic85/portfolio). This allows you to tailor the licensing conditions to better suit your specific requirements.

## Feature

* Is 100% Free - Open Source - Self Hosted and PWA!
* No download, plug-in, or login required, entirely browser-based
* Unlimited number of conference rooms without call time limitation
* Translated in 133 languages
* Host protection to ensure unauthorized access to your host
* Possibility to Password protect the Room for the meeting
* Desktop and Mobile compatible
* Optimized Room URL Sharing for mobile
* Webcam Streaming (Front - Rear for mobile)
* Audio Streaming crystal clear with detect speaking and volume indicator
* Screen Sharing to present documents, slides, and more...
* File Sharing (with drag-and-drop), share any files to your participants in the room
* Select Audio Input - Output and Video source
* Ability to set video quality up to 4K and 60 FPS
* Recording your Screen, Audio and Video
* Snapshot the video frame and save it as image png
* Chat with Emoji Picker to show you feeling, private messages, Markdown support, possibility to Save the conversations, and many more
* ChatGPT (openAI), designed to answer users' questions, provide relevant information, and connect them with relevant resources
* Speech recognition to send the speeches
* Push to talk, like a walkie-talkie.
* Advance collaborative whiteboard for the teachers
* Share any YT Embed video, video mp4, webm, ogg and audio mp3 in real-time
* Full-Screen Mode on mouse click on the Video element, Pin/Unpin, Zoom in-out video element
* Possibility to Change UI Themes
* Right-click on the Video elements for more options
* Direct peer-to-peer connection ensures the lowest latency thanks to WebRTC
* Supports REST API (Application Programming Interface)
* Slack API integration
* Sentry for error reporting
* ...
