#!/bin/bash

set -eu

cd /app/code

if [[ ! -f /app/data/config.js ]]; then
    echo "=> No config.js found, starting with new..."
    echo -e "module.exports = {};\n" > /app/data/config.js
fi

if [[ ! -f /app/data/env ]]; then
    SECRET=`openssl rand -hex 24`
    echo -e "# All options at https://github.com/miroslavpejic85/mirotalk/blob/master/.env.template \n" > /app/data/env
    echo -e "API_KEY_SECRET=${SECRET}" >> /app/data/env
    echo -e "" >> /app/data/env
    echo -e "# EMAIL_ALERT=true" >> /run/env
    echo -e "# EMAIL_SEND_TO=alertinbot@example.com" >> /run/env
    echo -e "" >> /app/data/env

    if [[ -n "${CLOUDRON_OIDC_ISSUER:-}" ]]; then
        cat >> /app/data/env <<EOT

# OIDC Auth via Cloudron SSO
# set to true if authentication is required for all routes (e.g. for invitaion links)
OIDC_AUTH_REQUIRED=false

EOT
    fi
fi

# bottom of https://meetrix.io/blog/webrtc/coturn/installation.html to generate username+password
time=$(date +%s)
expiry=512640 # one year in seconds
TURN_USERNAME=$(( $time + $expiry ))
TURN_PASSWORD=$(echo -n $TURN_USERNAME | openssl dgst -binary -sha1 -hmac ${CLOUDRON_TURN_SECRET} | openssl base64)
echo "Using TURN credentials: ${TURN_USERNAME} ${TURN_PASSWORD} for turn:${CLOUDRON_TURN_SERVER}:${CLOUDRON_TURN_PORT}"

# clean start
rm -f /run/env

echo -e "\n" >> /run/env
echo -e "# --- Current release template" >> /run/env
echo -e "\n" >> /run/env
cat /app/pkg/env.template >> /run/env
echo -e "\n" >> /run/env
echo -e "# --- Cloudron specific configs" >> /run/env
echo -e "\n" >> /run/env
echo -e "HOST=${CLOUDRON_APP_DOMAIN}"
echo -e "STUN_SERVER_ENABLED=true" >> /run/env
echo -e "STUN_SERVER_URL=stun:${CLOUDRON_STUN_SERVER}:${CLOUDRON_STUN_PORT}" >> /run/env
echo -e "TURN_SERVER_ENABLED=true" >> /run/env
echo -e "TURN_SERVER_URL=turn:${CLOUDRON_TURN_SERVER}:${CLOUDRON_TURN_PORT}" >> /run/env
echo -e "TURN_SERVER_USERNAME=${TURN_USERNAME}" >> /run/env
echo -e "TURN_SERVER_CREDENTIAL=${TURN_PASSWORD}" >> /run/env
echo -e "EMAIL_HOST=${CLOUDRON_MAIL_SMTP_SERVER}" >> /run/env
echo -e "EMAIL_PORT=${CLOUDRON_MAIL_SMTP_PORT}" >> /run/env
echo -e "EMAIL_USERNAME=${CLOUDRON_MAIL_SMTP_USERNAME}" >> /run/env
echo -e "EMAIL_PASSWORD=${CLOUDRON_MAIL_SMTP_PASSWORD}" >> /run/env

if [[ -n "${CLOUDRON_OIDC_ISSUER:-}" ]]; then

    SESSION_SECRET=`openssl rand -hex 24`
    cat >> /run/env <<EOT
# OIDC Auth via Cloudron SSO
TRUST_PROXY=true
OIDC_ENABLED=true
# CLOUDRON_OIDC_PROVIDER_NAME is not supported
OIDC_ISSUER_BASE_URL=${CLOUDRON_OIDC_ISSUER}
OIDC_BASE_URL=${CLOUDRON_APP_ORIGIN}
OIDC_CLIENT_ID='${CLOUDRON_OIDC_CLIENT_ID}'
OIDC_CLIENT_SECRET='${CLOUDRON_OIDC_CLIENT_SECRET}'
OIDC_AUTH_LOGOUT=false
SESSION_SECRET='${SESSION_SECRET}'
EOT
fi

echo -e "\n" >> /run/env
echo -e "# --- User configs" >> /run/env
cat /app/data/env >> /run/env

echo "=> Starting MiroTalk P2P"
exec /usr/local/bin/gosu cloudron:cloudron npm start
