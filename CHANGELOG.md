[0.1.0]
* Initial version

[0.2.0]
* Update to MiroTalk 1.2.77

[0.3.0]
* Update MiroTalk to 1.2.83

[0.4.0]
* Update MiroTalk to 1.2.84

[0.5.0]
* Update MiroTalk to 1.2.85

[0.6.0]
* Update MiroTalk to 1.2.87

[0.7.0]
* Update MiroTalk to 1.2.89

[1.0.0]
* First stable package version with MiroTalk 1.2.90

[1.0.1]
* Update MiroTalk to 1.2.91

[1.0.2]
* Update MiroTalk to 1.2.93

[1.0.3]
* Update MiroTalk to 1.2.94

[1.0.4]
* Update MiroTalk to 1.2.95

[1.0.5]
* Update MiroTalk P2P to 1.2.96

[1.0.6]
* Update MiroTalk P2P to 1.2.97

[1.0.7]
* Update MiroTalk P2P to 1.2.98

[1.1.0]
* Update MiroTalk P2P to 1.3.0

[1.1.1]
* Update MiroTalk p2p to 1.3.1

[1.1.2]
* Update MiroTalk p2p to 1.3.2

[1.1.3]
* Update MiroTalk p2p to 1.3.6

[1.1.4]
* Update MiroTalk p2p to 1.3.09

[1.1.5]
* Update MiroTalk p2p to 1.3.10

[1.1.6]
* Update MiroTalk p2p to 1.3.11

[1.1.7]
* Update MiroTalk p2p to 1.3.12

[1.1.8]
* Update MiroTalk p2p to 1.3.14

[1.1.9]
* Update MiroTalk p2p to 1.3.16

[1.1.10]
* Update MiroTalk p2p to 1.3.17

[1.1.11]
* Update MiroTalk p2p to 1.3.18

[1.1.12]
* Update MiroTalk p2p to 1.3.20
* Add optional email alert support

[1.1.13]
* Update MiroTalk p2p to 1.3.22

[1.1.14]
* Update MiroTalk p2p to 1.3.23

[1.1.15]
* Update MiroTalk p2p to 1.3.28

[1.1.16]
* Update MiroTalk p2p to 1.3.30

[1.1.17]
* Update MiroTalk p2p to 1.3.32

[1.1.18]
* Update MiroTalk p2p to 1.3.33

[1.1.19]
* Update MiroTalk p2p to 1.3.34

[1.1.20]
* Update MiroTalk p2p to 1.3.35

[1.1.21]
* Update MiroTalk p2p to 1.3.36

[1.1.22]
* Update MiroTalk p2p to 1.3.37

[1.1.23]
* Update MiroTalk p2p to 1.3.38

[1.1.24]
* Update MiroTalk p2p to 1.3.40

[1.1.25]
* Update MiroTalk p2p to 1.3.41

[1.1.26]
* Update MiroTalk p2p to 1.3.44

[1.1.27]
* Update MiroTalk p2p to 1.3.45

[1.1.28]
* Update MiroTalk p2p to 1.3.46

[1.1.29]
* Update MiroTalk p2p to 1.3.50

[1.1.30]
* Update MiroTalk p2p to 1.3.51

[1.1.31]
* Update MiroTalk p2p to 1.3.52

[1.1.32]
* Update MiroTalk p2p to 1.3.53

[1.1.33]
* Update MiroTalk p2p to 1.3.55

[1.1.34]
* Update MiroTalk p2p to 1.3.58

[1.1.35]
* Update MiroTalk p2p to 1.3.60

[1.1.36]
* Update MiroTalk p2p to 1.3.61

[1.1.37]
* Update MiroTalk p2p to 1.3.63

[1.1.38]
* Update MiroTalk p2p to 1.3.65

[1.1.39]
* Update MiroTalk p2p to 1.3.68

[1.1.40]
* Update MiroTalk p2p to 1.3.71

[1.1.41]
* Update MiroTalk p2p to 1.3.72

[1.1.42]
* Update MiroTalk p2p to 1.3.73

[1.1.43]
* Update MiroTalk p2p to 1.3.74

[1.1.44]
* Update MiroTalk p2p to 1.3.75

[1.1.45]
* Update MiroTalk p2p to 1.3.76

[1.1.46]
* Update MiroTalk p2p to 1.3.77

[1.1.47]
* Update MiroTalk p2p to 1.3.78

[1.1.48]
* Update MiroTalk p2p to 1.3.79

[1.1.49]
* Update MiroTalk p2p to 1.3.80

[1.1.50]
* Update MiroTalk p2p to 1.3.81

[1.1.51]
* Update MiroTalk p2p to 1.3.83

[1.1.52]
* Update MiroTalk p2p to 1.3.84

[1.1.53]
* Update MiroTalk p2p to 1.3.85

[1.1.54]
* Update MiroTalk p2p to 1.3.88

[1.1.55]
* Update MiroTalk p2p to 1.3.89

[1.1.56]
* Update mirotalkp2p to 1.3.90

[1.1.57]
* Update mirotalkp2p to 1.3.91

[1.1.58]
* Update mirotalkp2p to 1.3.92

[1.1.59]
* Update mirotalkp2p to 1.3.93

[1.1.60]
* Update mirotalkp2p to 1.3.94

[1.1.61]
* Update mirotalkp2p to 1.3.95

[1.1.62]
* Update mirotalkp2p to 1.3.96

[1.1.63]
* Update mirotalkp2p to 1.3.97

[1.1.64]
* Update mirotalkp2p to 1.3.98
* [Full Changelog](UNABLE-TO-DETECT-LINK)

[1.1.65]
* Update mirotalkp2p to 1.3.99

[1.2.0]
* Update mirotalkp2p to 1.4.10

[1.2.1]
* Update mirotalkp2p to 1.4.11

[1.2.2]
* Update mirotalkp2p to 1.4.12

[1.2.3]
* Update mirotalkp2p to 1.4.13

[1.2.4]
* Update mirotalkp2p to 1.4.16

[1.2.5]
* Update mirotalkp2p to 1.4.18

[1.2.6]
* Update mirotalkp2p to 1.4.20

[1.2.7]
* Update mirotalkp2p to 1.4.21

[1.2.8]
* Update mirotalkp2p to 1.4.22

[1.2.9]
* Update mirotalkp2p to 1.4.24

[1.2.10]
* Update mirotalkp2p to 1.4.26

[1.2.11]
* Update mirotalkp2p to 1.4.30

[1.2.12]
* Update mirotalkp2p to 1.4.34

[1.3.0]
* Update mirotalkp2p to 1.4.36
* Update node to 20.18

[1.3.1]
* Update mirotalkp2p to 1.4.38

[1.3.2]
* Update mirotalkp2p to 1.4.40

[1.3.3]
* Update mirotalkp2p to 1.4.42

[1.3.4]
* Update mirotalkp2p to 1.4.47

[1.3.5]
* Update mirotalkp2p to 1.4.50

[1.3.6]
* Update mirotalkp2p to 1.4.55

[1.3.7]
* Update mirotalkp2p to 1.4.57

[1.3.8]
* Update mirotalkp2p to 1.4.58

[1.3.9]
* Update mirotalkp2p to 1.4.59

[1.4.0]
* add multiDomain flag

[1.4.1]
* Update mirotalkp2p to 1.4.60

[1.4.2]
* Update mirotalkp2p to 1.4.61

[1.4.3]
* Update mirotalkp2p to 1.4.68

[1.4.4]
* Update mirotalkp2p to 1.4.69

[1.4.5]
* Update mirotalkp2p to 1.4.72

[1.4.6]
* Update mirotalkp2p to 1.4.75

[1.4.7]
* Update mirotalkp2p to 1.4.77

[1.4.8]
* Update mirotalkp2p to 1.4.79

[1.4.9]
* Update mirotalkp2p to 1.4.80

[1.4.10]
* OIDC auth implemented, tests updated

[1.4.11]
* Update mirotalkp2p to 1.4.81

[1.4.12]
* Update mirotalkp2p to 1.4.82

[1.15.0]
* Update base image to 5.0.0

[1.15.1]
* Update mirotalkp2p to 1.4.84

[1.15.2]
* Update mirotalkp2p to 1.4.85

