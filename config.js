// File will merge user config at /app/data/config.js with the upstream package config.template.js

const fs = require('fs');

console.log('=> Using Cloudron config patcher...')

function isObject(obj) {
  return obj !== null && typeof obj === 'object' && !Array.isArray(obj);
}

function merge(base, extra) {
  const output = { ...base };

  for (const key in extra) {
    if (extra.hasOwnProperty(key)) {
      const extraVal = extra[key];
      const targetVal = base[key];

      if (isObject(extraVal) && isObject(targetVal)) {
        output[key] = merge(targetVal, extraVal);
      } else {
        output[key] = isObject(extraVal) ? merge({}, extraVal) : extraVal;
      }
    }
  }

  return output;
}

const configTemplate = require('/app/code/app/src/config.template.js');
const userConfig = require('/app/data/config.js');

module.exports = merge(configTemplate, userConfig);
