<sso>
Cloudron users can login to Mirotalk.
If you need to make authentication is required for all routes (e.g. for invitaion links), you should set `OIDC_AUTH_REQUIRED=true` in `/app/data/env` using [Web Terminal](https://docs.cloudron.io/apps/#web-terminal).
</sso>
