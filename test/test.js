#!/usr/bin/env node

/* jshint esversion: 8 */
/* global it, xit, describe, before, after, afterEach */

'use strict';

require('chromedriver');

const execSync = require('child_process').execSync,
    fs = require('fs'),
    expect = require('expect.js'),
    path = require('path'),
    { Builder, By, Key, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

if (!process.env.USERNAME || !process.env.PASSWORD) {
    console.log('USERNAME, PASSWORD env vars need to be set');
    process.exit(1);
}

describe('Application life cycle test', function () {
    this.timeout(0);

    const LOCATION = process.env.LOCATION || 'test';
    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };
    const TEST_TIMEOUT = parseInt(process.env.TIMEOUT, 10) || 50000;

    let browser, app;

    const username = process.env.USERNAME;
    const password = process.env.PASSWORD;

    const displayName = 'Jessie Jones';
    const roomName = 'TestRoom';

    before(function () {
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1024 })
            .addArguments('use-fake-device-for-media-stream')
            .addArguments('use-fake-ui-for-media-stream');
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
        if (!fs.existsSync('./screenshots')) fs.mkdirSync('./screenshots');
    });

    after(function () {
        browser.quit();
    });

    afterEach(async function () {
        if (!process.env.CI || !app) return;

        const currentUrl = await browser.getCurrentUrl();
        if (!currentUrl.includes(app.domain)) return;
        expect(this.currentTest.title).to.be.a('string');

        const screenshotData = await browser.takeScreenshot();
        fs.writeFileSync(`./screenshots/${new Date().getTime()}-${this.currentTest.title.replaceAll(' ', '_')}.png`, screenshotData, 'base64');
    });

    async function waitForElement(elem) {
        await browser.wait(until.elementLocated(elem), TEST_TIMEOUT);
        await browser.wait(until.elementIsVisible(browser.findElement(elem)), TEST_TIMEOUT);
    }

    function getAppInfo() {
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location === LOCATION || a.location === LOCATION + '2'; })[0];
        expect(app).to.be.an('object');
    }

    async function loginOIDC(username, password, alreadyAuthenticated = false) {
        browser.manage().deleteAllCookies();
        await browser.get(`https://${app.fqdn}`);
        if (!alreadyAuthenticated) {
            await waitForElement(By.id('inputUsername'));
            await browser.findElement(By.id('inputUsername')).sendKeys(username);
            await browser.findElement(By.id('inputPassword')).sendKeys(password);
            await browser.findElement(By.id('loginSubmitButton')).click();
        }

        await browser.sleep(2000);

        // scroll one page down to element to reveal itself
        await waitForElement(By.xpath('//body'));
        await browser.findElement(By.xpath('//body')).sendKeys(Key.CONTROL, Key.END);
        await browser.sleep(2000);

        await waitForElement(By.id('roomName'));
    }

    async function createRoom() {
        await browser.get('https://' + app.fqdn);

        // scroll one page down to element to reveal itself
        await waitForElement(By.xpath('//body'));
        await browser.findElement(By.xpath('//body')).sendKeys(Key.CONTROL, Key.END);
        await browser.sleep(5000);

        await waitForElement(By.id('roomName'));
        await browser.findElement(By.id('roomName')).sendKeys(Key.CONTROL + 'a' + Key.COMMAND + 'a' + Key.BACK_SPACE);
        await browser.findElement(By.id('roomName')).sendKeys(roomName);
        await waitForElement(By.id('joinRoomButton'));
        await browser.findElement(By.id('joinRoomButton')).click();

        await browser.sleep(5000);

        const nameInput = By.xpath('//input[contains(@placeholder, "Enter your")]');
        await waitForElement(nameInput);
        await browser.findElement(nameInput).sendKeys(Key.CONTROL + 'a' + Key.COMMAND + 'a' + Key.BACK_SPACE);
        await browser.findElement(nameInput).sendKeys(displayName);

        await browser.sleep(2000);
        await waitForElement(By.xpath('//button[contains(., "Join meeting")]'));
        await browser.findElement(By.xpath('//button[contains(., "Join meeting")]')).click();

        await waitForElement(By.xpath('//h2[text()="Share the room"]'));
    }

    xit('build app', function () { execSync('cloudron build', EXEC_ARGS); });
    it('install app', function () { execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS); });

    it('can get app information', getAppInfo);

    it('can login via OIDC', loginOIDC.bind(null, username, password, false));
    it('can create Room', createRoom);

    it('can restart app', function () { execSync(`cloudron restart --app ${app.id}`, EXEC_ARGS); });

    it('can create Room', createRoom);

    it('backup app', function () { execSync(`cloudron backup create --app ${app.id}`, EXEC_ARGS); });
    it('restore app', async function () {
        await browser.get('about:blank');
        const backups = JSON.parse(execSync(`cloudron backup list --raw --app ${app.id}`));
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
        execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS);
        getAppInfo();
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
    });

    it('can create Room', createRoom);

    it('move to different location', async function () {
        await browser.get('about:blank');
        execSync(`cloudron configure --location ${LOCATION}2 --app ${app.id}`, EXEC_ARGS);
    });
    it('can get app information', getAppInfo);

    it('can create Room', createRoom);

    it('uninstall app', async function () {
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });

    // test update
    it('can install app', async function () { execSync(`cloudron install --appstore-id com.mirotalkp2p.cloudronapp --location ${LOCATION}`, EXEC_ARGS); });
    it('can get app information', getAppInfo);
//    it('can login via OIDC', loginOIDC.bind(null, username, password));

    it('can create Room', createRoom);

    it('can update', function () { execSync(`cloudron update --app ${app.id}`, EXEC_ARGS); });

    it('can create Room', createRoom);

    it('uninstall app', async function () {
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });
});

